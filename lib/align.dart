import 'package:flutter/material.dart';

void main() {
  runApp(const AlignExample());
}

class AlignExample extends StatelessWidget{
  const AlignExample({super.key});

  @override
  Widget build(BuildContext context){
    return MaterialApp(
      debugShowCheckedModeBanner:  false,
      home:Scaffold(
        body: Center(child: Container(
          height: 200.0,
        width: 600.0,
        color: Colors.red,
        child: Align(alignment: Alignment.topRight,
          child: Container(color: Colors.green[200],
          child:const Text('Align me!'),
          ),
          ),
        ),
        ),
      ),
    );
  }
}