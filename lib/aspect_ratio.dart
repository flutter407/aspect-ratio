import 'package:flutter/material.dart';

class AspectRatioExample extends StatelessWidget {
  const AspectRatioExample({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
         child: SizedBox(
           child: AspectRatio(
             aspectRatio: 1.5,
             child: Container(
               color: Colors.green[200],
               child: const FlutterLogo(),
             ),
           ),
         ),
       );
  }
}
